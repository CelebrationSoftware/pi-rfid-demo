import * as express from 'express';
import * as http from 'http';
const mfrc522 = require('mfrc522-rpi');
mfrc522.initWiringPi(0);

const app = express();

let connected: boolean = false;
let io: any = null;

//FIX up those CORS issues... couldn't figure out how to modify the ng serve to allow this.
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//SERVE up the Angular Code (should have already been previously built and inside its /dist folder)
app.use('/', express.static('../client/dist'));

//initialize a simple http server
const server = http.createServer(app);

//initialize the WebSocket server instance
const wss = require('socket.io')(server);

wss.on('connection', (socket: any) => {
    console.log('Someone connected');
    connected = true;

    io = socket;

    socket.on('message', (input:string) => {
        console.log('message recieved: ' + input);
        socket.emit('message', input);
    });

    socket.on('disconnect', () => {
        connected = false;
        console.log('Client disconnected');
    });

});

//start our server
server.listen(process.env.PORT || 8999, () => {
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    console.log('Websocket Server listening on ' + bind);
    startRFIDListener();
});

function startRFIDListener() {
    console.log("scanning...");
    console.log("Please put chip or keycard in the antenna inductive zone!");
    console.log("Press Ctrl-C to stop.");

    setInterval(function(){

        //# reset card
        mfrc522.reset();

        //# Scan for cards
        let response = mfrc522.findCard();
        if (!response.status) {
            console.log("No Card");
            return;
        }
        console.log("Card detected, CardType: " + response.bitSize);

        //# Get the UID of the card
        response = mfrc522.getUid();
        if (!response.status) {
            console.log("UID Scan Error");
            return;
        }
        //# If we have the UID, continue
        const uid = response.data;
        console.log("Card read UID: %s %s %s %s", uid[0].toString(16), uid[1].toString(16), uid[2].toString(16), uid[3].toString(16));
        const sUid = uid[0].toString(16) + '-' + uid[1].toString(16) + '-' + uid[2].toString(16) + '-' + uid[3].toString(16);

        //# Select the scanned card
        const memoryCapacity = mfrc522.selectCard(uid);
        console.log("Card Memory Capacity: " + memoryCapacity);

        //# This is the default key for authentication
        const key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF];

        //# Authenticate on Block 8 with key and uid
        if (!mfrc522.authenticate(8, key, uid)) {
            console.log("Authentication Error");
            return;
        }

        //# Dump Block 8
        console.log("Block: 8 Data: " + mfrc522.getDataForBlock(8));

        //# Stop
        mfrc522.stopCrypto();

        if (connected) {
            console.log('SENDING CARD to CONNECTED: ' + sUid);
            io.emit('readcard', sUid)
        } else {
            console.log('NOT CONNECTED: ' + sUid);
            return;
        }

    }, 500);

}