import { Component, OnInit } from '@angular/core';
import * as socketIO from 'socket.io-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  message: string;
  connected: boolean = false;
  giveIcecream: boolean = false;
  waiting: boolean = false;
  SERVER_URL: string = 'http://localhost:8999';
  socket = null;
  data: string[] = [];

  /***************************************************
   *
   */
  ngOnInit(): void {
    this.initIoConnection();
    this.doMessageUpdate();
  }

  /***************************************************
   *
   */
  initIoConnection(): void {
    this.socket = socketIO(this.SERVER_URL);
    var me = this;
    this.socket.on('connect', function() {
      me.ioConnected();
    });
    this.socket.on('message', function(input) {
      me.recievedMessage(input);
    });
    this.socket.on('readcard', function(input) {
      me.readCard(input);
    });
  }

  /***************************************************
   *
   */
  sendMessage(input:string): void {
    this.socket.emit('message', input);
  }

  /***************************************************
   *
   */
  ioConnected(): void {
    console.log('YES I AM CONNECTED NOW');
    this.connected = true;
    this.waiting = true;
    this.doMessageUpdate();
  }

  /***************************************************
   *
   */
  recievedMessage(message:string) {
    console.log('recievedMessage: ' + message);
  }

  /***************************************************
   *
   */
  readCard(message:string) {
    console.log('readCard: ' + message);

    //Do nothing if we're not waiting...
    if (!this.waiting) {
      return;
    }
    this.waiting = false;

    this.message = 'Recieved: ' + message;

    //Search for the message
    let f: boolean = false;

    for (let i = 0; i < this.data.length; i++){
      if (this.data[i] === message) {
        f = true;
        break;
      }

    }

    //Add this to our data...
    if(!f) {
      this.data.push(message);
    }

    this.giveIcecream = !f;
    this.doMessageUpdate();
  }

  /***************************************************
   *
   */
  myEvent(event): void {
      this.waiting = true;
      this.doMessageUpdate();
  }

  /***************************************************
   *
   */
  doMessageUpdate(): void {

    if(this.connected && this.waiting) {
      console.log('setting to waiting status');
      this.message = 'Waiting for a Hero Band';
    } else if (this.connected && !this.waiting) {

      if (this.giveIcecream) {
        this.message = 'GIVE ICECREAM!';
      } else {
        this.message = 'ALREADY HAS ICECREAM :)';
      }

    } else {
      this.message = 'Not Connected';
    }

    console.log('****** DATA DUMP *******');
    for (let i = 0; i < this.data.length; i++){
      console.log(this.data[i]);
    }

  }

}
