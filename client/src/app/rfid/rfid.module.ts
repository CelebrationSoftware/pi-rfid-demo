import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RfidComponent } from './rfid.component';
import { SocketService } from './shared/services/socket.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RfidComponent],
  providers: [SocketService]
})
export class RfidModule { }
