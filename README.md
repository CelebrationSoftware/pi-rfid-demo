# Description
Quick Magic Band with Entitlement demonstration.

# Hardware
Raspberry Pi 3 b/b+

RFID RC522


# Installing and running

## Prerequisites
Install Node: http://thisdavej.com/beginners-guide-to-installing-node-js-on-a-raspberry-pi/
Ensure Reader works:  http://www.instructables.com/id/RFID-RC522-Raspberry-Pi/

```
sudo npm i -g typescript
sudo npm i -g @angular/cli

git clone https://gitlab.com/CelebrationSoftware/pi-rfid-demo.git

cd ./client
npm install
ng build prod

cd ../server
npm install
tsc
node ./dist/server/server.js
```

# Links
Description | Link
--- | ---
node javascript RFIC interface | https://www.npmjs.com/package/mfrc522-rpi
instructables example | http://www.instructables.com/id/RFID-RC522-Raspberry-Pi/
installing node on pi | http://thisdavej.com/beginners-guide-to-installing-node-js-on-a-raspberry-pi/
typescript node express websocket: https://hackernoon.com/nodejs-web-socket-example-tutorial-send-message-connect-express-set-up-easy-step-30347a2c5535
